package com.example.app001food.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.food002.R
import com.example.food002.models.FoodModel

class FoodMenuAdapter(var user: (FoodModel) -> Unit) :
    RecyclerView.Adapter<FoodMenuAdapter.ViewHolder>() {
    private var list: List<FoodModel> = arrayListOf()
    fun setFoodList(list: List<FoodModel>) {
        this.list = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_two, parent, false)
        return ViewHolder(view) {
            user.invoke(it)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
    }

    class ViewHolder(view: View, var user: (FoodModel) -> Unit) : RecyclerView.ViewHolder(view) {

        var imageFood: ImageView = view.findViewById(R.id.country_photo)
        var imageFov: ImageView = view.findViewById(R.id.image_icon_star)
        var textFood: TextView = view.findViewById(R.id.country_name)

        fun bind(foodModel: FoodModel) {
            textFood.text = foodModel.name
            when (foodModel.id) {
                1 -> imageFood.setImageResource(R.drawable.image_02)
                2 -> imageFood.setImageResource(R.drawable.image_03)
                3 -> imageFood.setImageResource(R.drawable.image_04)
                else -> imageFood.setImageResource(R.drawable.image_1)
            }
            when (foodModel.favorites) {
                0 -> imageFov.setImageResource(R.drawable.ic_star_border)
                1 -> imageFov.setImageResource(R.drawable.ic_star)
            }

            imageFov.setOnClickListener {
                when (foodModel.check) {
                    0 -> {
                        imageFov.setImageResource(R.drawable.ic_star)
                        foodModel.check = 1
                    }
                    1 -> {
                        imageFov.setImageResource(R.drawable.ic_star_border)
                        foodModel.check = 0
                    }
                }
                user.invoke(foodModel)
            }
        }
    }
}