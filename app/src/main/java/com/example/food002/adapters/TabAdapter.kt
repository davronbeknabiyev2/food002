package com.example.app001food.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter


class TabAdapter(var fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager) {
    var fragmentList: List<Fragment> = arrayListOf()
    var fragmentListTitle: List<String> = arrayListOf()
    override fun getItem(position: Int): Fragment {
        return fragmentList[position]
    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        (fragmentList as ArrayList).add(fragment)
        (fragmentListTitle as ArrayList).add(title)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return fragmentListTitle.get(position)
    }

}