package com.example.food002.datadase

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.example.food002.models.FoodModel
import com.example.food002.models.User

class DBController(var context: Context) {

    private var dbHelper: DBHelper
    var database: SQLiteDatabase

    init {
        dbHelper = DBHelper(context)
        database = dbHelper.writableDatabase
    }

    companion object {
        const val DB_NAME = "food.db"
        const val DB_VERSION = 1
        const val USER_TABLE_NAME = "user"
        const val USER_ID = "id"
        const val USER_NAME = "name"
        const val USER_EMAIL = "email"
        const val USER_IMAGE_URI = "image_uri"
        const val USER_TOKEN = "token"

        const val CREATE_USER_TABLE =
            "CREATE TABLE $USER_TABLE_NAME( $USER_ID INTEGER PRIMARY KEY, $USER_NAME TEXT, $USER_EMAIL TEXT, $USER_IMAGE_URI TEXT, $USER_TOKEN TEXT );"

        const val FOOD_TABLE_NAME = "food_menu"
        const val FOOD_ID = "id"
        const val FOOD_NAME = "name"
        const val FOOD_IMAGE_URI = "image_uri"
        const val FOOD_FAVORITES = "favorites"

        const val CREATE_FOOD_TABLE =
            "CREATE TABLE $FOOD_TABLE_NAME( $FOOD_ID INTEGER PRIMARY KEY, $FOOD_NAME TEXT, $FOOD_IMAGE_URI TEXT, $FOOD_FAVORITES INTEGER);"
    }

    class DBHelper(var context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {
        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(CREATE_USER_TABLE)
            db?.execSQL(CREATE_FOOD_TABLE)

            db?.execSQL("INSERT INTO $USER_TABLE_NAME($USER_NAME, $USER_EMAIL, $USER_IMAGE_URI, $USER_TOKEN) VALUES('admin','admin','0','123456789');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Soups','0','0');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Pasta','0','0');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Pizza','0','0');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Chicken','0','0');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Risto','0','0');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Vego Rolls','0','0');")
            db?.execSQL("INSERT INTO $FOOD_TABLE_NAME($FOOD_NAME, $FOOD_IMAGE_URI, $FOOD_FAVORITES) VALUES('Tandoor','0','0');")
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

        }

    }

    fun checkUser(name: String, email: String): Int {
        val cursor =
            database.rawQuery(
                "select id from $USER_TABLE_NAME where $USER_NAME=? and $USER_EMAIL=?",
                arrayOf(name, email)
            )
        if (cursor.moveToFirst()) {
            return cursor.getInt(cursor.getColumnIndex("id"))
        }
        return -1
    }

    fun saveUser(user: User): Int {
        val checkUser = checkUser(user.name, user.email)
        if (checkUser == -1) {
            val contentValues = ContentValues()
            contentValues.put(USER_NAME, user.name)
            contentValues.put(USER_EMAIL, user.email)
            contentValues.put(USER_IMAGE_URI, user.imageUri)
            contentValues.put(USER_TOKEN, user.token)
            val toInt = database.insert(USER_TABLE_NAME, null, contentValues).toInt()
            Log.d("TTT", "MyDatabase saveUser: $toInt")
            return toInt
        }
        return checkUser
    }

    fun getFoodList(): List<FoodModel> {
        val list: List<FoodModel> = arrayListOf()
        val c = database.rawQuery("select * from food_menu", null)
        if (c.moveToFirst()) {
            do {
                var foodModel = FoodModel(
                    c.getInt(c.getColumnIndex(FOOD_ID)),
                    c.getString(c.getColumnIndex(FOOD_NAME)),
                    c.getString(c.getColumnIndex(FOOD_IMAGE_URI)),
                    c.getInt(c.getColumnIndex(FOOD_FAVORITES))
                )
                (list as ArrayList).add(foodModel)
            } while (c.moveToNext())
        }
        return list
    }

    fun setFavoritesFood(id: Int) {
        var cursor =
            database.rawQuery("select $FOOD_FAVORITES from $FOOD_TABLE_NAME where id='$id'", null)
        var fav = -1
        if (cursor.moveToFirst()) {
            fav = cursor.getInt(cursor.getColumnIndex("favorites"))
        }
        Log.d("TTT", "favorites: $fav")
        var contentValues = ContentValues()
        if (fav == 0) {
            contentValues.put(FOOD_FAVORITES, 1)
        } else {
            contentValues.put(FOOD_FAVORITES, 0)
        }
        database.update(FOOD_TABLE_NAME, contentValues, "id='$id'", null)
    }

    fun getUserId(id: Int): User {
        val user: User
        val cursor = database.rawQuery("select * from $USER_TABLE_NAME where id='$id'", null)
        if (cursor.moveToFirst()) {
            user = User(
                cursor.getInt(cursor.getColumnIndex(USER_ID)),
                cursor.getString(cursor.getColumnIndex(USER_NAME)),
                cursor.getString(cursor.getColumnIndex(USER_EMAIL)),
                cursor.getString(cursor.getColumnIndex(USER_IMAGE_URI)),
                cursor.getString(cursor.getColumnIndex(USER_TOKEN))
            )
            return user
        }
        return null!!
    }
}