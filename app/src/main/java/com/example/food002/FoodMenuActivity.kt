package com.example.food002

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.viewpager.widget.ViewPager
import com.example.app001food.adapters.TabAdapter
import com.example.food002.models.User
import com.example.food002.ui.foodmenu.FoodMenuContract
import com.example.food002.ui.foodmenu.FoodMenuModel
import com.example.food002.ui.foodmenu.FoodMenuPresenter
import com.example.food002.ui.foodmenu.fragments.ListFoodMenuFragment
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat


class FoodMenuActivity : AppCompatActivity(), FoodMenuContract.View {

    lateinit var adapter: TabAdapter

    private val fragmentLatest = ListFoodMenuFragment(1)
    private val fragmentFavorites = ListFoodMenuFragment(2)
    lateinit var presenter: FoodMenuContract.Presenter
    private var userId: Int = -1

    lateinit var navView: NavigationView
    lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_menu)

        findId()

        view_pager_food.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                Log.d("TTT", "onPageSelected position: $position")
                when (position) {
                    0 -> {
                        fragmentLatest.presenter.init()
                    }
                    1 -> {
                        fragmentFavorites.presenter.init()
                    }
                }
            }
        })

        presenter = FoodMenuPresenter(this, FoodMenuModel(this))
        presenter.inti()
    }

    private fun findId() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "FoodMenu"

        var intent: Intent = intent

        this.userId = intent.getIntExtra("userId", -1)
        Log.d("BB", "FoodMenuActivity onCreate: ${this.userId}")

        adapter = TabAdapter(supportFragmentManager)

        adapter.addFragment(fragmentLatest, "Latest")
        adapter.addFragment(fragmentFavorites, "Favorites")
        var viewPager: ViewPager = findViewById(R.id.view_pager_food)
        viewPager.adapter = adapter
        tab_layout.setupWithViewPager(viewPager)

        drawerLayout = findViewById(R.id.drawer_layout)
        navView = findViewById(R.id.nav_view)

        var toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.setDrawerListener(toggle)
        toggle.syncState()
    }

    override fun getUserId() = userId

    override fun setUser(user: User) {

        val hView = navView.getHeaderView(0)
        var imageView: ImageView = hView.findViewById(R.id.image_nav_header_image)
        var textName: TextView = hView.findViewById(R.id.text_nav_header_name)
        var textEmail: TextView = hView.findViewById(R.id.text_nav_header_email)

        textName.text = user.name
        textEmail.text = user.email

        Picasso.get()
            .load(user.imageUri)
            .into(imageView)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_log_out -> {
                FirebaseAuth.getInstance().signOut()
                startActivity(Intent(this, SignInActivity::class.java))
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}
