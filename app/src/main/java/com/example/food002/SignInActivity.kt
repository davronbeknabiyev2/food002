package com.example.food002

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.food002.ui.signin.SignInContract
import com.example.food002.ui.signin.SignInModel
import com.example.food002.ui.signin.SignInPresenter
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.activity_main.*

class SignInActivity : AppCompatActivity(), SignInContract.View {

    lateinit var presenter: SignInContract.Presenter

    companion object {
        const val USER_ID = "userId"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = SignInPresenter(this, SignInModel(this))

        Log.d("TTT", "SignInActivity onCreate: ")

        button_sign_in.setOnClickListener {
            Log.d("TTT", "SignInActivity button_sign_in: ")
            presenter.signIn()
        }

        button_google.setOnClickListener {
            Log.d("TTT", "SignInActivity button_google: ")
            presenter.signInGoogle()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("TTT", "SignInActivity onActivityResult: ")
        presenter.activityResult(requestCode, resultCode, data)
    }

    override fun getName() = edit_name.text.toString()

    override fun getEmail() = edit_email.text.toString()

    override fun success(userId: Int) {
        var intent = Intent(this, FoodMenuActivity::class.java)
        intent.putExtra(USER_ID, userId)
        startActivity(intent)
        finish()
    }

    override fun error(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        progress_layout.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_layout.visibility = View.INVISIBLE
    }

}
