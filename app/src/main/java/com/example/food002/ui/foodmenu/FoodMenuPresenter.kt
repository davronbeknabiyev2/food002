package com.example.food002.ui.foodmenu

import java.lang.Exception

class FoodMenuPresenter(var view: FoodMenuContract.View, var model: FoodMenuContract.Model) :
    FoodMenuContract.Presenter {

    override fun inti() {
        view.setUser(model.getUser(view.getUserId()))
    }

    override fun checkFavorites() {

    }
}