package com.example.food002.ui.signin

import android.content.Intent
import com.example.food002.models.User

interface SignInContract {
    interface Model {
        fun checkUser(name: String, email: String): Int
        fun saveUser(user: User): Int

    }

    interface View {
        fun getName(): String
        fun getEmail(): String
        fun success(userId: Int)
        fun error(massage: String)
        fun showProgress()
        fun hideProgress()
    }

    interface Presenter {
        fun signIn()
        fun signInGoogle()
        fun activityResult(requestCode: Int, resultCode: Int, data: Intent?)
    }
}