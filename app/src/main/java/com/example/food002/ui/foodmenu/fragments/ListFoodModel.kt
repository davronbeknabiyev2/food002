package com.example.food002.ui.foodmenu.fragments

import android.content.Context
import com.example.food002.datadase.DBController
import com.example.food002.models.FoodModel

class ListFoodModel(context: Context) :
    ListFoodModelContract.Model {
    var myDBHelper = DBController(context)

    override fun getLatestList(): List<FoodModel> {
        return myDBHelper.getFoodList()
    }

    override fun getFavoritesList(): List<FoodModel> {
        var list = arrayListOf<FoodModel>()
        myDBHelper.getFoodList().forEach { food ->
            if (food.favorites == 1) {
                list.add(food)
            }
        }
        return list
    }

    override fun checkFavoritesItem(foodId: Int) {
        myDBHelper.setFavoritesFood(foodId)
    }
}