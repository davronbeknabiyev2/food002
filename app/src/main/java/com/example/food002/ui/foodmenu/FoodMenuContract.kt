package com.example.food002.ui.foodmenu

import com.example.food002.models.User

interface FoodMenuContract {
    interface Model {
        fun getUser(id: Int): User
    }

    interface View {
        fun setUser(user: User)
        fun getUserId(): Int
    }

    interface Presenter {
        fun checkFavorites()
        fun inti()
    }
}