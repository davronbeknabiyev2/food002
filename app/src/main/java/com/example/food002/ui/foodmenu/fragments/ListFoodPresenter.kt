package com.example.food002.ui.foodmenu.fragments

class ListFoodPresenter(
    var view: ListFoodModelContract.View,
    var model: ListFoodModelContract.Model
) : ListFoodModelContract.Presenter {

    override fun init() {
        if (view.getInitIdFood() == 1) {
            view.setList(model.getLatestList())
        } else if (view.getInitIdFood() == 2) {
            view.setList(model.getFavoritesList())
        }
    }

    override fun checkFavorites() {
        val food = view.getCheckedFood()
        model.checkFavoritesItem(food.id)
        init()
    }
}