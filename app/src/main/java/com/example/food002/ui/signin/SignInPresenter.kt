package com.example.food002.ui.signin

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.food002.R
import com.example.food002.SignInActivity
import com.example.food002.models.User
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider


class SignInPresenter(var view: SignInContract.View, var model: SignInContract.Model) :
    SignInContract.Presenter {

    var googleSignInClient: GoogleSignInClient
    var mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private var context: Context = view as Context
    var token: String = ""

    companion object {
        const val GOOGLE_OK = 123
    }

    init {
        if (mAuth.currentUser != null) {
            val user = mAuth.currentUser
            updateUi(user!!)

        }
        val gso: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(context.getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        googleSignInClient = GoogleSignIn.getClient(context, gso)
    }

    override fun signIn() {
        var name = view.getName()
        var email = view.getEmail()
        view.showProgress()
        val checkUser = model.checkUser(name, email)
        view.hideProgress()
        Log.d("TTT", "SignInPresenter signIn: $checkUser")
        if (checkUser == -1) {
            view.error("Name or Email error!!!")
        } else {
            view.success(checkUser)
        }
    }

    override fun signInGoogle() {
        Log.d("TTT", "SignInPresenter signInGoogle:")
        view.showProgress()
        var intent = googleSignInClient.signInIntent
        (context as Activity).startActivityForResult(intent, GOOGLE_OK)
    }

    override fun activityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d("TTT", "SignInPresenter activityResult:")
        if (requestCode == GOOGLE_OK) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account = task.getResult(ApiException::class.java)
                if (account != null)
                    firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                Log.w("TAG", "Google sign in failed", e);
            }
        }
    }

    fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        token = acct.idToken.toString()
        Log.d("TTT", "SignInPresenter firebaseAuthWithGoogle: $token")
        var credential: AuthCredential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth.signInWithCredential(credential).addOnCompleteListener(context as Activity) {
            if (it.isSuccessful) {
                view.hideProgress()
                var f = mAuth.currentUser
                updateUi(f!!)
            } else {
                view.hideProgress()
                view.error("Authentication failed.")
            }
        }
    }

    fun updateUi(user: FirebaseUser) {
        if (user != null) {
            val name = user.displayName.toString()
            val email = user.email.toString()
            val imageUri = user.photoUrl.toString()

            Log.d("TTT", "SignInPresenter updateUi: $name")
            Log.d("TTT", "SignInPresenter updateUi: $email")

            var u = User(0, name, email, imageUri, token)
            val saveUser = model.saveUser(u)

            Log.d("TTT", "SignInPresenter updateUi: $saveUser")
            view.success(saveUser)
        }
    }


}