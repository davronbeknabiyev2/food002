package com.example.food002.ui.foodmenu

import android.content.Context
import com.example.food002.datadase.DBController
import com.example.food002.models.User

class FoodMenuModel(context: Context) : FoodMenuContract.Model {
    var myDBHelper = DBController(context)

    override fun getUser(id: Int): User {
        return myDBHelper.getUserId(id)
    }
}