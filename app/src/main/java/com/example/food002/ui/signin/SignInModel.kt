package com.example.food002.ui.signin

import android.content.Context
import android.util.Log
import com.example.food002.datadase.DBController
import com.example.food002.models.User

class SignInModel(var context: Context) : SignInContract.Model {
    var myDatabase = DBController(context)

    override fun checkUser(name: String, email: String): Int {
        return myDatabase.checkUser(name, email)
    }

    override fun saveUser(user: User): Int {
        Log.d("TTT", "SignInModel saveUser: ")
        return myDatabase.saveUser(user)
    }
}