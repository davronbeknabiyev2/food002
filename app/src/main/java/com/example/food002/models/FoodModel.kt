package com.example.food002.models

data class FoodModel(
    var id: Int,
    var name: String,
    var image_uri: String,
    var favorites: Int,
    var check: Int = favorites
)