package com.example.food002.models

data class User(
    var id: Int,
    var name: String,
    var email: String,
    var imageUri: String,
    var token: String
)